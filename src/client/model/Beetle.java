package client.model;

public class Beetle extends Entity {
    private Direction direction;
    private BeetleType beetleType;
    private int directionInt = -1;
    private int color = -1;
    private int queen = -1;
    private int sick = -1;
    private int team = -1;
    private int power = 0;


    public Beetle(int id) {
        super(id, EntityType.Beetle);
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        switch (color) {
            case 0:
                this.beetleType = BeetleType.LOW;
                break;
            case 1:
                this.beetleType = BeetleType.HIGH;
                break;
        }
        this.color = color;
    }

    public int getDirectionInt() {
        return this.directionInt;
    }

    public void setQueen(int queen) {
        this.queen = queen;
    }

    public void setSick(int sick) {
        this.sick = sick;
    }

    public int getTeam() {
        return team;
    }

    public void setTeam(int team) {
        this.team = team;
    }

    public boolean is_sick() {
        return (sick == 1);
    }

    public Direction getDirection() {
        return this.direction;
    }

    public void setDirection(int direction) {
        switch (direction) {
            case 0:
                this.direction = Direction.Right;
                break;
            case 1:
                this.direction = Direction.Up;
                break;
            case 2:
                this.direction = Direction.Left;
                break;
            case 3:
                this.direction = Direction.Down;
                break;
        }
        this.directionInt = direction;
    }

    public BeetleType getBeetleType() {
        return this.beetleType;
    }

    public boolean has_winge() {
        return (queen == 1);
    }


}