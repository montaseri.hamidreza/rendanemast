package client;

import client.model.*;

/*
*    TODO: change beetle types more intelligent, based on which frequency is better for the beetle
*    TODO: Can we find another view to the game?
*    TODO: Use of deterministic move (if the default move is very bad for the beetle, especially queens)
*    TODO: Improve the constants that we used to calculate points
*    TODO: Prevent beetles to stay still
*    TODO: (points matter)  Take care of queens
*    !!!Give Every Battle a property if it is going to move
 */

public class AI {
    static int[][][][] strategy = new int[2][3][2][3];

    public void doTurn(World game) {
        updateTypes(game);
        int[][][][] s = findStrategy(game);
//        for (int i = 0; i < 2; i++) {
//            for (int j = 0; j < 3; j++) {
//                for (int k = 0; k < 2; k++) {
//                    for (int l = 0; l < 3; l++) {
//                            System.out.print(s[i][j][k][l]+" ");
//                    }
//                }
//            }
//        }
//        System.out.println("");
        updateStrategy(game, s);
        //doDetMoves(game);
    }

    private void doDetMoves(World g) {
        int ms = g.getMyScore(), os = g.getOppScore(), cc = 1;
        for (Cell c : g.getMap().getMyCells()) {
            if (c != null && ((Beetle) c.getBeetle()).has_winge()
                    && (ms > os + cc * g.getConstants().getDetMoveCost())) {
                Beetle b = (Beetle) c.getBeetle();
                double[] p = analyzeMoves(g, b);
                int s[] = getSituation(g, b);
                if (p[strategy[b.getBeetleType().getValue()][s[0]][s[1]][s[2]]] < 0) {
                    double max = Double.MIN_VALUE;
                    int indmax = 1;
                    for (int m = 0; m < 3; m++) {
                        if (p[m] > max) {
                            max = p[m];
                            indmax = m;
                        }
                    }
                    if (p[indmax] > 0) {
                        g.deterministicMove(b, Move.values()[indmax]);
                        cc++;
                    }
                }
            }
        }
    }

    private double[] analyzeMoves(World g, Beetle b) {
        double[] r = new double[3];
        r[0] = calValue(g, b, Move.turnRight);
        r[1] = calValue(g, b, Move.stepForward);
        r[2] = calValue(g, b, Move.turnLeft);
        return r;
    }

    private void updateStrategy(World g, int[][][][] s) {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 3; j++) {
                for (int k = 0; k < 2; k++) {
                    for (int l = 0; l < 3; l++) {
                        if (s[i][j][k][l] != strategy[i][j][k][l])
                            g.changeStrategy(BeetleType.values()[i],
                                    CellState.values()[j],
                                    CellState.values()[k],
                                    CellState.values()[l],
                                    Move.values()[s[i][j][k][l]]);
                    }
                }
            }
        }
        strategy = s;
    }

    public int[][][][] findStrategy(World g) {
        double[][][][][] up = new double[2][3][2][3][3];

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 3; j++) {
                for (int k = 0; k < 2; k++) {
                    for (int l = 0; l < 3; l++) {
                        for (int m = 0; m < 3; m++) {
                            up[i][j][k][l][m] = 0;
                        }
                    }
                }
            }
        }

        //Analyse moves for each of our beetles
        for (Cell c : g.getMap().getMyCells()) {
            if (c != null && c.getBeetle() != null) {
                analyzeMoves(g, (Beetle) c.getBeetle(), up);
            }
        }
        //Find best move for each situation
        int[][][][] ret = new int[2][3][2][3];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 3; j++) {
                for (int k = 0; k < 2; k++) {
                    for (int l = 0; l < 3; l++) {
                        double max = -1000000;
                        int indmax = 1;
                        for (int m = 1; m <= 3; m++) {
                            if (up[i][j][k][l][m%3] > max) {
                                max = up[i][j][k][l][m%3];
                                indmax = m%3;
                            }
                        }
                        //System.out.println("");
                        ret[i][j][k][l] = indmax;
                    }

                }
            }
        }
        //System.out.println("");
        return ret;
    }

    public void analyzeMoves(World g, Beetle b, double[][][][][] up) {
        int[] p = getSituation(g, b);
        up[b.getBeetleType().getValue()][p[0]][p[1]][p[2]][0] += calValue(g, b, Move.turnRight);
        up[b.getBeetleType().getValue()][p[0]][p[1]][p[2]][1] += (calValue(g, b, Move.stepForward)+5);
        up[b.getBeetleType().getValue()][p[0]][p[1]][p[2]][2] += calValue(g, b, Move.turnLeft);
    }
    //DONE
    public int[] getSituation(World g, Beetle b) {
        Map m = g.getMap();
        int h = m.getHeight(), w = m.getWidth();
        Cell c1 = new Cell(0, 0), c2 = new Cell(0, 0), c3 = b.getPosition();
        if (b.getDirection() == Direction.Down) {
            c1 = m.getCell((b.getRow() + 1) % h, (b.getColumn() - 1 + w) % w);
            c2 = m.getCell((b.getRow() + 1) % h, (b.getColumn() + 1) % w);
            for (int i = 1; i <= h; i++) {
                if (m.getCell((b.getRow() + i) % h, b.getColumn()).getBeetle() != null) {
                    c3 = m.getCell((b.getRow() + i) % h, b.getColumn());
                    break;
                }
            }
        }
        if (b.getDirection() == Direction.Up) {
            c1 = m.getCell((b.getRow() - 1 + h) % h, (b.getColumn() + 1) % w);
            c2 = m.getCell((b.getRow() - 1 + h) % h, (b.getColumn() - 1 + w) % w);
            for (int i = 1; i <= h; i++) {
                if (m.getCell((b.getRow() - i + h) % h, b.getColumn()).getBeetle() != null) {
                    c3 = m.getCell((b.getRow() - i + h) % h, b.getColumn());
                    break;
                }
            }
        }
        if (b.getDirection() == Direction.Left) {
            c1 = m.getCell((b.getRow() - 1 + h) % h, (b.getColumn() - 1 + w) % w);
            c2 = m.getCell((b.getRow() + 1) % h, (b.getColumn() - 1 + w) % w);
            for (int i = 1; i <= w; i++) {
                if (m.getCell(b.getRow(), (b.getColumn() - i + w) % w).getBeetle() != null) {
                    c3 = m.getCell(b.getRow(), (b.getColumn() - i + w) % w);
                    break;
                }
            }
        }
        if (b.getDirection() == Direction.Right) {
            c1 = m.getCell((b.getRow() + 1) % h, (b.getColumn() + 1) % w);
            c2 = m.getCell((b.getRow() - 1 + h) % h, (b.getColumn() + 1) % w);
            for (int i = 1; i <= w; i++) {
                if (m.getCell(b.getRow(), (b.getColumn() + i) % w).getBeetle() != null) {
                    c3 = m.getCell(b.getRow(), (b.getColumn() + i) % w);
                    break;
                }
            }
        }
        int[] ret = new int[3];
        if (c1.getBeetle() == null)
            ret[0] = 2;
        else if (((Beetle) c1.getBeetle()).getTeam() == b.getTeam())
            ret[0] = 0;
        else
            ret[0] = 1;
        if (((Beetle) c3.getBeetle()).getTeam() == b.getTeam())
            ret[1] = 0;
        else
            ret[1] = 1;
        if (c2.getBeetle() == null)
            ret[2] = 2;
        else if (((Beetle) c2.getBeetle()).getTeam() == b.getTeam())
            ret[2] = 0;
        else
            ret[2] = 1;
        //System.out.println(b.getRow()+" "+b.getColumn()+"\n"+ret[0]+ret[1]+ret[2]);
        return ret;
    }

    public double calValue(World g, Beetle beetle, Move m) {
        int h = g.getMap().getHeight(), w = g.getMap().getWidth();
        //Do the move virtually
        Beetle b = copy(beetle);
        if (m == Move.stepForward) {
            if (b.getDirection().getValue() % 2 == 1)
                b.getPosition().setRow((b.getPosition().getRow() + b.getDirection().getValue() - 2 + h) % h);
            else
                b.getPosition().setColumn((b.getPosition().getColumn() + 1 - b.getDirection().getValue() + w) % w);
            //Do teleport if available
            Cell tel = g.getMap().getCell(b.getColumn(), b.getRow());
            if (tel.getTeleport() != null) {
                b.getPosition().setColumn(tel.getTeleport().getColumn());
                b.getPosition().setRow(tel.getTeleport().getRow());
            }
        }

        //Calculate points
        double ret = 0.0;
        ret += calFoodPoint(g, b);
        ret += calSlipperPoint(g, b);
        ret += calPoisonPoint(g, b);
//        ret += calSicknessPoint(g, b);
        if(m == Move.stepForward) //otherwise we can't do anything about it
            ret += calFightPoint(g, b);
        return  ret;//TODO analyze again
    }

    private double calFightPoint(World g, Beetle b) {
        int h = g.getMap().getHeight(), w = g.getMap().getWidth(), p1 = 0, p2 = 0, sh1 = 0, sh2 = 0, ma1 = 0, ma2 = 0;
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                Cell c = g.getMap().getCell((b.getRow() + i + h) % h, (b.getColumn() + j + w) % w);
                if (c.getBeetle() != null && calDistance(g, b.getPosition(), (Beetle) c.getBeetle()) < 2) {
                    if (((Beetle) c.getBeetle()).getTeam() == b.getTeam()) {
                        p1 += ((Beetle) c.getBeetle()).getPower();
                        if (((Beetle) c.getBeetle()).has_winge())
                            sh1 += 1;
                        else
                            ma1 += 1;
                    } else {
                        p2 += ((Beetle) c.getBeetle()).getPower();
                        if (((Beetle) c.getBeetle()).has_winge())
                            sh2 += 1;
                        else
                            ma2 += 1;
                    }
//                    if (i == 0 && j == 0) {
//                        if (((Beetle) c.getBeetle()).getTeam() == b.getTeam())
//                            p1 += 2 * ((Beetle) c.getBeetle()).getPower();
//                        else
//                            p2 += 2 * ((Beetle) c.getBeetle()).getPower();
//                        if (((Beetle) c.getBeetle()).has_winge())
//                            sh2 += 3;
//                        else
//                            ma2 += 3;
//                    }

                }
            }
        }
//        p1 += b.getPower();
//        if (b.has_winge())
//            sh1++;
//        else
//            ma1++;
        //TODO: Check
        if ((sh2+ma2)  < 1)
            return 0;
        if (p1 > 2 * p2)
            return sh2 * 250 + ma2 * 100;
        if (p2 > 2 * p1)
            return -1*(b.has_winge()?250:100);
        return 0;
    }

    private double calSicknessPoint(World g, Beetle b) {
        int h = g.getMap().getHeight(), w = g.getMap().getWidth(), t = 0;
        Cell p = g.getMap().getCell(b.getRow(),b.getColumn());
        if (b.is_sick() || p.getTrashEntity()!=null) {
            for (int i = -1; i < 2; i++) {
                for (int j = -1; j < 2; j++) {
                    if (j == 0 && i == 0)
                        continue;
                    Cell c = g.getMap().getCell((b.getRow() + i + h) % h, (b.getColumn() + j + w) % w);
                    if (c.getBeetle() != null && !((Beetle) c.getBeetle()).is_sick()) {
                        if (((Beetle) c.getBeetle()).getTeam() == b.getTeam())
                            t--;
                        else
                            t++;
                    }
                }
            }
        } else {
            for (int i = -1; i < 2; i++) {
                for (int j = -1; j < 2; j++) {
                    if (j == 0 && i == 0)
                        continue;
                    Cell c = g.getMap().getCell((b.getRow() + i + h) % h, (b.getColumn() + j + w) % w);
                    if (c.getBeetle() != null && ((Beetle) c.getBeetle()).is_sick())
                        t--;
                }
            }
        }
        return t * 10;
    }

    private double calSlipperPoint(World g, Beetle b) {
        double rs = 0, cons = 100.0;
        int d = b.getDirection().getValue(), r = b.getRow(), c = b.getColumn(),
                h = g.getMap().getHeight(), w = g.getMap().getWidth();
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                Slipper s = (Slipper) g.getMap().getCell((r + i + h) % h, (c + j + w) % w).getSlipper();
                if (s == null)
                    continue;
                if (i == 0 && j == 0)
                    rs = Math.max(rs, cons * 2 / (s.getRemainingTurns() + 1));
                else if (i == 0 || j == 0) {
                    int p = Math.abs(i) * (i + 2) + Math.abs(j) * (j + 1);
                    rs = Math.max(rs, cons * (3 - Math.abs(d - p)) / (s.getRemainingTurns() + 1));
                } else {
                    if ((d - 2) == i || (d - 1) == j)
                        rs = Math.max(rs, cons * 2 / (s.getRemainingTurns() + 1));
                    else
                        rs = Math.max(rs, cons * 1 / (s.getRemainingTurns() + 1));
                }
            }
        }
        return -rs*(b.has_winge()?2.5:1);
    }

    public double calFoodPoint(World g, Beetle b) {
        int m = -1;
        Cell[] fc = g.getMap().getFoodCells();
        for (Cell c : fc) {
            if (calDistance(g, c, b) <= ((Food) c.getFoodEntity()).getRemainingTurns()) {
//                System.out.println(calDistance(g, c, b));
                m = (m<0?calDistance(g,c,b):Math.min(m, calDistance(g, c, b)));
            }
        }
        m++;
        //System.out.println(""+ m +" " +(double)100/(m*m));
        if(m>10 || m<1 || (g.getMap().getMyCells().length*10)>(g.getMap().getHeight()*g.getMap().getWidth()*3))
            return 0;
        return (b.has_winge()?2:1)*(double) 100 / (m * m);
    }

    public double calPoisonPoint(World g, Beetle b) {
        Cell c = g.getMap().getCell(b.getRow(), b.getColumn());
        double maxPower=0;
        if(c.getTrashEntity() == null || b.is_sick())
            return 0;
        for(Cell cc:g.getMap().getMyCells()){
            maxPower = Math.max(((Beetle)cc.getBeetle()).getPower(),maxPower);
        }
        return b.getPower()/2;//(-30.0*b.getPower()/maxPower)*(b.has_winge()?2:1);
    }
    //TODO: Update
    private void updateTypes(World game) {
        int[][] t = new int[2][2];
        for (Cell c : game.getMap().getMyCells()) {
            if (c == null)
                continue;
            Beetle b = (Beetle) c.getBeetle();
            if (b.getBeetleType() == BeetleType.LOW) {
                if (b.has_winge())
                    t[0][0]++;
                else
                    t[0][1]++;
            } else {
                if (b.has_winge())
                    t[1][0]++;
                else
                    t[1][1]++;
            }
        }
        //System.out.println(t[0][0] + " " + t[0][1] + " " + t[1][0] + " " + t[1][1]);
        if (t[0][0] + t[0][1] - t[1][0] - t[1][1] > 1) {
            if (t[0][0] - t[1][0] > 1)
                changeABeetleType(game, BeetleType.LOW, true);
            else
                changeABeetleType(game, BeetleType.LOW, false);
        } else if (t[1][0] + t[1][1] - t[0][0] - t[0][1] > 1) {
            if (t[1][0] - t[0][0] > 1)
                changeABeetleType(game, BeetleType.HIGH, true);
            else
                changeABeetleType(game, BeetleType.HIGH, false);
        }

    }

    private void changeABeetleType(World game, BeetleType t, boolean hw) {
        //System.out.println(t.name() + " " + hw);
        for (Cell c : game.getMap().getMyCells()) {
            if (c == null || c.getBeetle() == null)
                continue;
            Beetle b = (Beetle) c.getBeetle();
            if (b.has_winge() == hw && b.getBeetleType() == t) {
                game.changeType(b, (BeetleType.values()[0] == t ? BeetleType.values()[1] : BeetleType.values()[0]));
                break;
            }

        }
    }
    //DONE
    public int calDistance(World g, Cell c, Beetle b) {
        int h = g.getMap().getHeight(), w = g.getMap().getWidth();
        int d = b.getDirection().getValue();
        int a1 = (c.getColumn() - b.getColumn() + w) % w + (c.getRow() - b.getRow() + h) % h + 1;
        if (b.getDirection() == Direction.Left || b.getDirection() == Direction.Up)
            a1++;
        else if (b.getColumn() == c.getColumn() || b.getRow() == c.getRow())
            a1--;

        int a2 = (b.getColumn() - c.getColumn() + w) % w + (c.getRow() - b.getRow() + h) % h + 1;
        if (b.getDirection() == Direction.Right || b.getDirection() == Direction.Up)
            a2++;
        else if (b.getColumn() == c.getColumn() || b.getRow() == c.getRow())
            a2--;

        int a3 = (c.getColumn() - b.getColumn() + w) % w + (b.getRow() - c.getRow() + h) % h + 1;
        if (b.getDirection() == Direction.Left || b.getDirection() == Direction.Down)
            a3++;
        else if (b.getColumn() == c.getColumn() || b.getRow() == c.getRow())
            a3--;

        int a4 = (b.getColumn() - c.getColumn() + w) % w + (b.getRow() - c.getRow() + h) % h + 1;
        if (b.getDirection() == Direction.Right || b.getDirection() == Direction.Down)
            a4++;
        else if (b.getColumn() == c.getColumn() || b.getRow() == c.getRow())
            a4--;
        int r = Math.min(Math.min(a1, a2), Math.min(a3, a4));
        return r;
    }

    //DONE
    private Beetle copy(Beetle b) {
        Beetle r = new Beetle(b.getId());
        r.setCell(new Cell(b.getRow(), b.getColumn()));
        r.setTeam(b.getTeam());
        r.setDirection(b.getDirection().getValue());
        r.setSick((b.is_sick() ? 1 : 0));
        r.setPower(b.getPower());
        return r;
    }

}